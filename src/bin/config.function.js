// Require some Node.js packages
import _ from 'lodash';
import fs from 'fs';
import yaml from 'js-yaml';
import rimraf from 'rimraf';
import { exec } from 'child_process';

/**
 * @function: Read all configuration files
 * @param path: Location for configuration directory
 * @param fileList: Files in configuration directory
 * @returns {Array}: List of all configuration files
 */
function getConfigFile(path, fileList = []) {
    let files = fs.readdirSync(path);
    _.forEach(files, file => {
        if (fs.statSync(path + '/' + file).isDirectory()) {
            fileList = getConfigFile(dir + file + '/', fileList);
        } else {
            if (/^.+yml$/.test(file)) {
                fileList.push(file);
            }
        }
    });
    return fileList;
}

/**
 * @function: Read configuration file content
 * @param path: Configuration file
 * @returns {JSON}: Convert YAML configuration to JSON object
 */
function getConfiguration(path) {
    return yaml.load(fs.readFileSync(path, 'utf-8'));
}

/**
 * @function: Delete current modules folder
 * @param path: Location for modules
 */
function deleteFolder(path) {
    if (fs.existsSync(path)) {
        rimraf.sync(path);
    }
}

/**
 * @function: Create folder
 * @param path: Folder path
 */
function createDirectorySync(path) {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path, err => {
            err
                ? console.log(`${path} could not be created`)
                : console.log(`${path} successfully created`);
        });
    }
}

/**
 * @function: Create file
 * @param path: File path
 */
function createFileSync(path) {
    if (!fs.existsSync(path)) {
        fs.writeFileSync(path, '', err => {
            err
                ? console.log(`${path} could not be created`)
                : console.log(`${path} successfully created`);
        });
    }
}

/**
 * @function: Add content to file
 * @param path: File path
 * @param content: Content to be add to file
 */
function addContentToFile(path, content) {
    if (!fs.existsSync(path)) {
        createFileSync(path);
    }
    fs.writeFileSync(path, content, err => {
        err
            ? console.log(`Content could not be added to ${path}`)
            : console.log(`Successfully add content to ${path}`);
    });
}

/**
 *
 * @param dependency: extra dependencies required for schema
 */
function installDependencies(dependency) {
    let dependencies = '';
    for (let i in dependency) {
        dependencies += dependency[i] + ' ';
    }
    exec(`npm install ${dependencies}`, err => {
        err
            ? console.log(`${dependencies} could not be installed`)
            : console.log(`${dependencies} successfully installed`);
    });
}

// Export the functions to be accessible when the JS File is required
module.exports = {
    getConfigFile,
    getConfiguration,
    deleteFolder,
    createDirectorySync,
    createFileSync,
    addContentToFile,
    installDependencies,
};
