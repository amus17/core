import {
    convertDecimalToWGS84,
    convertWGS84ToDecimal,
    generateSipHash,
} from './RESTParser.main.js';

test('Testing GeoPoint conversion from decimal to degree', () => {
    const latitude = 38.907192;
    const longitude = -77.036871;
    const convertedGeoPoint = convertDecimalToWGS84(longitude, latitude, true);

    expect(convertedGeoPoint.longitude).toBe('77° 2\' 12,74" W');
    expect(convertedGeoPoint.latitude).toBe('38° 54\' 25,89" N');
});

test('Testing GeoPoint conversion from degree to decimal', () => {
    const latitude = '38° 54\' 25.89" N';
    const longitude = '77° 2\' 12.74" W';
    const convertedGeoPoint = convertWGS84ToDecimal(longitude, latitude, true);

    expect(convertedGeoPoint.longitude).toBe(-77.03687222222221);
    expect(convertedGeoPoint.latitude).toBe(38.90719166666666);
});

test('Testing GeoPoint conversion from decimal to degree', () => {
    const _ID = 22402;
    const objecID = 1;
    const source = 'helcom';

    const hashValue = generateSipHash(_ID, objecID, source);

    expect(hashValue).toBe(7100615770988584);
    expect(String(hashValue)).toBe('7100615770988584');
});
