// Require some Node.js packages
import _ from 'lodash';
// Import axios to request data from RESTFul endpoint
import axios from 'axios';
// Import GeoPoint class for GeoPoint conversion
import GeoPoint from '../geopoint/geopoint';

// Require SipHash package to generate unique hash integer
const siphash = require('siphash');
// SipHash key for hashing messages (if changed, different values will be generated)
const siphashKey = siphash.string16_to_key('&C7/1^=5"cjtUsM!AE6@|mM.1x8V\\;gw');

// Require XML2JS package to convert XML to JSON object
import xml2js from 'xml2js';
const xml2jsParser = new xml2js.Parser({
    explicitArray: false,
});

/**
 *
 * @param attributes: Unknown number of parameter to generate the SipHash based on them
 * @returns {*}: Hashed value from the input parameteres and the siphashKey above
 */
function generateSipHash(...attributes) {
    // Build one string from the attributes
    let attributesConcat = _.join(attributes, '');
    return siphash.hash_uint(siphashKey, attributesConcat);
}

/**
 *
 * @param longitude: Longitude in WGS84 format
 * @param latitude: latitude in WGS84 format
 * @param latitude: Decimal separator (default is the dot)
 * @returns {{longitude: null, latitude: null}}
 */
function convertWGS84ToDecimal(longitude, latitude, commaDecimal = false) {
    let convertedGeoPoint = {
        longitude: null,
        latitude: null,
    };
    // Create new point object from GeoPoint class
    const geoPoint = new GeoPoint(
        commaDecimal ? _.replace(longitude, ',', '.') : longitude,
        commaDecimal ? _.replace(latitude, ',', '.') : latitude
    );
    convertedGeoPoint.longitude = geoPoint.getLonDec();
    convertedGeoPoint.latitude = geoPoint.getLatDec();
    return convertedGeoPoint;
}

/**
 *
 * @param longitude: Longitude in decimal format
 * @param latitude: Latitude in decimal format
 * @param latitude: Decimal separator (default is the dot)
 * @returns convertedGeoPoint: WGS84 encoded GeoPoint
 */
function convertDecimalToWGS84(longitude, latitude, commaDecimal = false) {
    let convertedGeoPoint = {
        longitude: null,
        latitude: null,
    };
    // Create new point object from GeoPoint class
    const geoPoint = new GeoPoint(longitude, latitude);
    convertedGeoPoint.longitude = commaDecimal
        ? _.replace(geoPoint.getLonDeg(), '.', ',')
        : geoPoint.getLonDeg();
    convertedGeoPoint.latitude = commaDecimal
        ? _.replace(geoPoint.getLatDeg(), '.', ',')
        : geoPoint.getLatDeg();
    return convertedGeoPoint;
}

/**
 *
 * @param result: Result object from REST endpoint
 * @param dataLocation: Location of resolved data inside result (separated with ->)
 * @returns {*}: Object containing requested data
 */
function reduceResult(result, dataLocation) {
    // Check if result is not an array
    if (!_.isArray(result)) {
        // return the desired location or null in case it can't be located
        return _.has(result, dataLocation) ? result[dataLocation] : null;
    } else {
        let returnResult = [];
        // Push desired location in empty array and return it
        _.forEach(result, data => {
            returnResult.push(data[dataLocation] || null);
        });
        return _.concat([], returnResult);
    }
}

/**
 *
 * @param res: Result object from REST endpoint
 * @param sourceMapping: Filed mapping between REST endpoint and schema defined (optional)
 * @param sourceResolve: Fixed values or Filter functions for requested data (optional)
 * @param dataLocation: Location of requested data (optional)
 * @returns {Array}: Map, filter and locate (if required) data
 */
function parseData(
    res,
    sourceMapping = null,
    sourceResolve = null,
    dataLocation = null
) {
    let resolvedData = [];

    // Check if result exists
    if (!_.isUndefined(res)) {
        // Locate the data recursively from result
        if (dataLocation !== null) {
            _.forEach(_.split(dataLocation, '->'), location => {
                res = reduceResult(res, location);
            });
        }
        // Check if result is not an array
        if (!_.isArray(res)) {
            // Build empty object and cached object to be returned
            let obj = {},
                cachedObj = {};
            // Loop over result keys
            _.forEach(res, (value, key) => {
                let objectKey = key;
                // Change object key if there is mapping for it
                if (!_.isNull(sourceMapping) && _.has(sourceMapping, key)) {
                    objectKey = sourceMapping[key];
                }
                // Add result to obj
                let objectValue = value;
                obj[objectKey] = objectValue;
            });

            // Check if fixed values or fileter functions exists for the schema
            if (!_.isNull(sourceResolve)) {
                // Clone built object into cached
                _.assign(cachedObj, obj);
                _.forEach(sourceResolve, (value, key) => {
                    let objectKey = key;
                    let objectValue = '';
                    // Check if the configured resolve is a fixed value and not a function
                    if (!_.isObject(value) || value.type !== 'function') {
                        // Save fixed value to its key
                        objectValue = value;
                        obj[objectKey] = objectValue;
                    } else {
                        if (_.isObject(value) && value.type == 'function') {
                            // Define the function body with help of the eval function
                            let currentFilter = function(data) {
                                return executeFilter(data);

                                function executeFilter(data) {
                                    return eval(value.definition);
                                }
                            };
                            // Save filtered value to its key
                            objectValue = currentFilter(cachedObj);
                            obj[objectKey] = objectValue;
                        }
                    }
                });
            }
            // Push the built object to the parsed data
            resolvedData.push(obj);

            // Check if result is not an array
        } else if (_.isArray(res)) {
            // Loop over every array element (element should be an JSON object)
            _.forEach(res, data => {
                // Build empty object and cached object to be returned
                let obj = {},
                    cachedObj = {};

                // Loop over result keys
                _.forEach(data, (value, key) => {
                    let objectKey = key;
                    // Change object key if there is mapping for it
                    if (!_.isNull(sourceMapping) && _.has(sourceMapping, key)) {
                        objectKey = sourceMapping[key];
                    }
                    // Add result to obj
                    let objectValue = value;
                    obj[objectKey] = objectValue;
                });

                // Check if fixed values or fileter functions exists for the schema
                if (!_.isNull(sourceResolve)) {
                    // Clone built object into cached
                    _.assign(cachedObj, obj);
                    _.forEach(sourceResolve, (value, key) => {
                        let objectKey = key;
                        let objectValue = '';
                        // Check if the configured resolve is a fixed value and not a function
                        if (!_.isObject(value) || value.type !== 'function') {
                            // Save fixed value to its key
                            objectValue = value;
                            obj[objectKey] = objectValue;
                        } else {
                            if (_.isObject(value) && value.type == 'function') {
                                // Define the function body with help of the eval function
                                let currentFilter = function(data) {
                                    return executeFilter(data);

                                    function executeFilter(data) {
                                        return eval(value.definition);
                                    }
                                };
                                // Save filtered value to its key
                                objectValue = currentFilter(cachedObj);
                                obj[objectKey] = objectValue;
                            }
                        }
                    });
                }
                // Push the built object to the parsed data
                resolvedData.push(obj);
            });
        }
    }
    // Resturn parsed data
    return resolvedData;
}

/**
 *
 * @param sourceURL: Source URL endpoint
 * @param sourceMapping: Filed mapping between REST endpoint and schema defined (optional)
 * @param sourceResolve: Fixed values or Filter functions for requested data (optional)
 * @param dataLocation: Location of requested data (optional)
 * @returns {Promise<Array>}: Map, filter and locate (if required) data
 */
function resolveDataFromREST(
    sourceURL,
    sourceMapping = null,
    sourceResolve = null,
    dataLocation = null
) {
    // Request data from endpoint with axios
    return axios.get(sourceURL).then(res => {
        // result is always located in res.data (axios default)
        return parseData(res.data, sourceMapping, sourceResolve, dataLocation);
    });
}

/**
 *
 * @param sourceURL: Source URL endpoint
 * @param sourceMapping: Filed mapping between REST endpoint and schema defined (optional)
 * @param sourceResolve: Fixed values or Filter functions for requested data (optional)
 * @param dataLocation: Location of requested data (optional)
 * @returns {Promise<Array>}: Map, filter and locate (if required) data
 */
function resolveDataFromRESTWithXML(
    sourceURL,
    sourceMapping = null,
    sourceResolve = null,
    dataLocation = null
) {
    // Request data from endpoint with axios
    return axios.get(sourceURL).then(res => {
        // result is always located in res.data (axios default)
        let convertedResult = null;
        xml2jsParser.parseString(res.data, (err, result) => {
            if (err) {
                return err;
            } else {
                convertedResult = result;
            }
        });
        return parseData(
            convertedResult,
            sourceMapping,
            sourceResolve,
            dataLocation
        );
    });
}

// Export function to be used in other JS and JS test files
module.exports = {
    resolveDataFromREST,
    resolveDataFromRESTWithXML,
    convertDecimalToWGS84,
    convertWGS84ToDecimal,
    generateSipHash,
};
