# Changelog

## [Unreleased]

| **`Expected release date`** | **`compaire with v1.0`**                                   |
| --------------------------- | ---------------------------------------------------------- |
| 31/09/2018                  | https://gitlab.com/tuc_graphql/core/compare/v1.0...staging |

## [1.0]

| **`Release date`** | **`compaire with v0.1`**                                | **`compaire with v0.2`**                                | **`Compaire with v0.5`**                                | **`compaire with v0.6`**                                | **`compaire with v0.7`**                                | **`compaire with v0.8`**                                | **`compaire with v0.8.1`**                                | **`compaire with v0.9`**                                |
| ------------------ | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | --------------------------------------------------------- | ------------------------------------------------------- |
| 10/08/2018         | https://gitlab.com/tuc_graphql/core/compare/v0.1...v1.0 | https://gitlab.com/tuc_graphql/core/compare/v0.2...v1.0 | https://gitlab.com/tuc_graphql/core/compare/v0.5...v1.0 | https://gitlab.com/tuc_graphql/core/compare/v0.6...v1.0 | https://gitlab.com/tuc_graphql/core/compare/v0.7...v1.0 | https://gitlab.com/tuc_graphql/core/compare/v0.8...v1.0 | https://gitlab.com/tuc_graphql/core/compare/v0.8.1...v1.0 | https://gitlab.com/tuc_graphql/core/compare/v0.9...v1.0 |

### Changed

### Added

-   Add SAST, DAST and container scanning in CI. 85!

## [0.9]

| **`Release date`** | **`compaire with v0.1`**                                | **`compaire with v0.2`**                                | **`Compaire with v0.5`**                                | **`compaire with v0.6`**                                | **`compaire with v0.7`**                                | **`compaire with v0.8`**                                | **`compaire with v0.8.1`**                                |
| ------------------ | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | --------------------------------------------------------- |
| 21/07/2018         | https://gitlab.com/tuc_graphql/core/compare/v0.8...v0.9 | https://gitlab.com/tuc_graphql/core/compare/v0.8...v0.9 | https://gitlab.com/tuc_graphql/core/compare/v0.5...v0.9 | https://gitlab.com/tuc_graphql/core/compare/v0.6...v0.9 | https://gitlab.com/tuc_graphql/core/compare/v0.7...v0.9 | https://gitlab.com/tuc_graphql/core/compare/v0.8...v0.9 | https://gitlab.com/tuc_graphql/core/compare/v0.8.1...v0.9 |

### Changed

-   Optimize code with lodash. 82!
-   Add modules folder to git. 82!

## [0.8.1]

| **`Release date`** | **`compaire with v0.1`**                                  | **`compaire with v0.2`**                                  | **`Compaire with v0.5`**                                  | **`compaire with v0.6`**                                  | **`compaire with v0.7`**                                  | **`compaire with v0.8`**                                  |
| ------------------ | --------------------------------------------------------- | --------------------------------------------------------- | --------------------------------------------------------- | --------------------------------------------------------- | --------------------------------------------------------- | --------------------------------------------------------- |
| 11/06/2018         | https://gitlab.com/tuc_graphql/core/compare/v0.1...v0.8.1 | https://gitlab.com/tuc_graphql/core/compare/v0.2...v0.8.1 | https://gitlab.com/tuc_graphql/core/compare/v0.5...v0.8.1 | https://gitlab.com/tuc_graphql/core/compare/v0.6...v0.8.1 | https://gitlab.com/tuc_graphql/core/compare/v0.7...v0.8.1 | https://gitlab.com/tuc_graphql/core/compare/v0.8...v0.8.1 |

### Changed

-   Optimize documentation. 82!
-   Update dependencies. 82!
-   Update changelog. 82!

## [0.8]

| **`Release date`** | **`compaire with v0.1`**                                | **`compaire with v0.2`**                                | **`Compaire with v0.5`**                                | **`compaire with v0.6`**                                | **`compaire with v0.7`**                                |
| ------------------ | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- |
| 29/05/2018         | https://gitlab.com/tuc_graphql/core/compare/v0.1...v0.8 | https://gitlab.com/tuc_graphql/core/compare/v0.2...v0.8 | https://gitlab.com/tuc_graphql/core/compare/v0.5...v0.8 | https://gitlab.com/tuc_graphql/core/compare/v0.6...v0.8 | https://gitlab.com/tuc_graphql/core/compare/v0.7...v0.8 |

### Changed

-   Simplify schema configuration files. 78!
-   Adjust documentation. 78!

### Added

-   Implement global resolvers for REST, XML and PostgreSQL. 78!
-   Create two more configuration samples for XML and PostgreSQL. 78!

## [0.7]

| **`Release date`** | **`Compaire with v0.1`**                                | **`compaire with v0.2`**                                | **`compaire with v0.5`**                                | **`compaire with v0.6`**                                |
| ------------------ | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- |
| 22/05/2018         | https://gitlab.com/tuc_graphql/core/compare/v0.1...v0.7 | https://gitlab.com/tuc_graphql/core/compare/v0.2...v0.7 | https://gitlab.com/tuc_graphql/core/compare/v0.5...v0.7 | https://gitlab.com/tuc_graphql/core/compare/v0.6...v0.7 |

### Changed

-   Add schema secrect to the schema configuration file. 75!
-   Add two schema configuration sample files. 75!

### Added

-   Step by step configuration to create dynamic schema from configuration file. 75!

## [0.6]

| **`Release date`** | **`Compaire with 0.1`**                                 | **`compaire with v0.2`**                                | **`Compaire with 0.5`**                                 |
| ------------------ | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- |
| 18/05/2018         | https://gitlab.com/tuc_graphql/core/compare/v0.1...v0.6 | https://gitlab.com/tuc_graphql/core/compare/v0.2...v0.6 | https://gitlab.com/tuc_graphql/core/compare/v0.5...v0.6 |

### Changed

-   Restructure the server core folder. !65
-   GraphQL SDL - Schema Definition Language using [SchemaGlue](https://github.com/nicolasdao/schemaglue). !65
-   Explain new folder structure in documentation. !65

### Added

-   First version of dynamic server configuration. !73

## [0.5]

| **`Release date`** | **`Compaire with 0.1`**                                 | **`Compaire with 0.2`**                                 |
| ------------------ | ------------------------------------------------------- | ------------------------------------------------------- |
| 22/04/2018         | https://gitlab.com/tuc_graphql/core/compare/v0.1...v0.5 | https://gitlab.com/tuc_graphql/core/compare/v0.2...v0.5 |

### Changed

-   Request data from specific source. !40
-   License. !62
-   Code coverage with Jest. !62
-   Restructure folders and sub-folders. !63

### Added

-   Changelog and badges. !42
-   Code check with prettier. !49
-   Code coverage with karma. !49
-   Data model to GitLab pages. !62
-   Dependency Scanning. !63
-   Browser Performance Testing. !63
-   Contribution guide. !63

## [0.2]

| **`Release date`** | **`Compaire with 0.1`**                                 |
| ------------------ | ------------------------------------------------------- |
| 16/02/2018         | https://gitlab.com/tuc_graphql/core/compare/v0.1...v0.2 |

### Changed

-   Request detection by name. !11
-   Optimize code and ignore files. !13, !23, !24

### Added

-   Continuous Integration on GitLab and Deploy on Heroku. !8
-   Code documentation. !10
-   Wiki documentation and GitLab pages. !25

## [0.1]

| **`Release date`** |
| ------------------ |
| 15/01/2018         |

### Added

-   Add NodeJS project structure. !11
-   Implement simple detection request using GraphQL. !11

[unreleased]: https://gitlab.com/tuc_graphql/core/commits/staging
[0.9]: https://gitlab.com/tuc_graphql/core/tags/0.9
[0.8.1]: https://gitlab.com/tuc_graphql/core/tags/0.8.1
[0.8]: https://gitlab.com/tuc_graphql/core/tags/0.8
[0.7]: https://gitlab.com/tuc_graphql/core/tags/0.7
[0.6]: https://gitlab.com/tuc_graphql/core/tags/0.6
[0.5]: https://gitlab.com/tuc_graphql/core/tags/0.5
[0.2]: https://gitlab.com/tuc_graphql/core/tags/0.2
[0.1]: https://gitlab.com/tuc_graphql/core/tags/0.1
