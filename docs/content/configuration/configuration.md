---
title: "Configuration"
date: 2018-05-18T02:03:32+02:00
author: "Abram Lawendy"
menu: main
type: configuration
weight: 40
draft: false
---

### Table of contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

-   [Modules](#modules)
-   [Configuration Path](#configuration-path)
-   [Configuration Structure](#configuration-structure)
-   [Configuration Example](#configuration-example)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### Modules

There is two way to add a module in the project.<br />
The first possibility ba adding a YAML schema configuration file in `configuration`.<br />
The second possibility is to create a folder inside `src/modules` for the desired module.

### Configuration Path

All configuration files must be saved in the `configuration` folder and their name must end with `.yml` extension.<br />
Other files in the `configuration` folder will be ignored.<br />
The folder/ configuration file name for each module must be unique otherwise modules with the same name will be overridden.

### Configuration Structure

The configuration file must consist of the following:

| **`Key`**              | **`Description`**                                              | **`Required`** |
| ---------------------- | -------------------------------------------------------------- | -------------- |
| name                   | Name for the schema and GraphQL query                          | `Yes`          |
| description            | Schema description                                             | `No`           |
| secret                 | Schema secret variables                                        | `No`           |
| field                  | Fields and data type for the schema                            | `Yes`          |
| resolver               | Resolver for the defined query                                 | `Yes`          |
| source                 | REST-Endpoint to resolve the schema                            | `No`           |
| source -> name         | Name of the REST-Endpoint                                      | `Yes`          |
| source -> url          | URL of the REST-Endpoint                                       | `Yes`          |
| source -> mapping      | Mapping between fields of the schema and REST-Endopoint fields | `Yes`          |
| source -> resolve      | Assign filed of schema a static value or function              | `Yes`          |
| source -> dataLocation | Location of requested data in returned data from source        | `Yes`          |

### Configuration Example

```yaml
# Unique identifier to the schema (OBLIGATORY)
name: detection
# schema description (OPTIONAL)
description: Locations of positive detections of chemical warfare agents in sediments samples
# Secrets to be save in the schema configuration file "config/config.json" (OBLIGATORY)
secret:
  TOKEN:
  HOST:
  USER:
  PASSWORD:
# Schema definition in GraphQL Syntax (OBLIGATORY)
field:
  type detectionType {
        _ID: String
        objectID: String
        name: String
        details: String
        latitude: Float
        longitude: Float
        hashValue: ID
        source: String
    }

    type Query {
        detection: [detectionType]
    }
# Resolver for the data(OBLIGATORY)
resolver: >
  // Import general parser for RESTFul endpoints

  import { resolveDataFromREST, } from 'class/parser/RESTParser.main';

  // Import schema configuration from file and save it in variable CONFIG

  import CONFIG from 'modules/detection/config/config.json';

  // Function to parse data from source URL

  function detectionParser(CONFIG, schemaName) {
        const sources = CONFIG['source'];
        let promises = [];

        for (let i in sources) {
            // Save requested data (Promises) in array
            promises.push(
                resolveDataFromREST(
                    encodeURI(sources[i].url),
                    sources[i]['mapping'][schemaName] || null,
                    sources[i]['resolve'][schemaName] || null,
                    sources[i]['dataLocation'][schemaName] || null
                )
            );
        }

        // Resolve promises and return the data

        return Promise.all(promises).then(responds => {
            let data = [];
            for (let i = 0; i < responds.length; i++) {
                // Push results to one array
                for (let j = 0; j < responds[i].length; j++) {
                    data.push(responds[i][j]);
                }
            }
            return data;
        });
    }

  //  Define the queries resolvers with help of the general parser function for REST

  exports.resolver = {
      Query: {
          detection(root) {
              // Pass the schema configuration and the requested query

              return detectionParser(CONFIG, 'detection');
          },
      },
  };
# Source to resolve the schema
source:
  # Saved as array (OBLIGATORY)
  1:
    name: chemsea
    url: http://153.19.108.112/arcgis/rest/services/chemsea/MapServer/1/query?f=json&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=objectid ASC&outSR=102100&resultOffset=0
    # Mapping between REST endpoint fields (LEFT) and schema fileds (RIGHT)
    mapping:
      # Query name as defined in the schema (OBLIGATORY)
      detection:
        objectid: objectID
        name: name
        punpch_p_opis: details
        punpch_p_szer_wgs84: latitude
        punpch_p_dl_wgs84: longitude
    # Fixed values or Filter functions for requested data (optional)
    resolve:
      # Query name as defined in the schema (OBLIGATORY)
      detection:
        # Always resolve _ID to 1 from this source
        _ID: 1
        name: Sulfur mustard
        source: chemsea
        # Pass hashValue value to a filter function
        hashValue:
          # Type of filter is a function (OBLIGATORY)
          type: function
          # Function body without return (OBLIGATORY)
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
        longitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
        latitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
    # Location of requested data (OPTIONAL)
    dataLocation:
      detection: features->attributes
  2:
    name: chemsea
    url: http://153.19.108.112/arcgis/rest/services/chemsea/MapServer/2/query?f=json&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=objectid ASC&outSR=102100&resultOffset=0
    mapping:
      detection:
        objectid: objectID
        name: name
        punpch_p_opis: details
        punpch_p_szer_wgs84: latitude
        punpch_p_dl_wgs84: longitude
    resolve:
      detection:
        _ID: 2
        name: Adamsite
        source: chemsea
        hashValue:
          type: function
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
        longitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
        latitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
    dataLocation:
      detection: features->attributes
```

This sample configuration file can be downloaded from [here](../../data/detection.config.yml).
